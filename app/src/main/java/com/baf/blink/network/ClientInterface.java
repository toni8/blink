package com.baf.blink.network;

import com.baf.blink.models.Category;
import com.baf.blink.models.City;
import com.baf.blink.models.Market;
import com.baf.blink.models.Order;
import com.baf.blink.models.Product;
import com.baf.blink.utils.Constants;

import java.security.Provider;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import retrofit2.http.Streaming;
import retrofit2.http.Url;

/**
 * Created by Antonio on 20/02/2017.
 */

public interface ClientInterface {

    @GET(Constants.URL_CITIES)
    Call<City[]> getCities();

    @GET(Constants.URL_CATEGORIES)
    Call<Category[]> getCategories();

    @GET(Constants.URL_MARKETS_BY_CITY)
    Call<Market[]> getMarketsByCity(@Query("cityId") String cityId);

    @GET(Constants.URL_PRODUCTS)
    Call<Product[]> getProducts();

    @POST(Constants.URL_ORDER)
    Call<ResponseBody> postOrder(@Body Order order);

  /*  //validate token
    @POST(Constants.BACK_END_TOKEN_LOGIN)
    Call<byte[]> validateToken(@Body LoginModel loginModel);

    //add bill
    @POST(Constants.ADD_BILL_URL)
    Call<UserSsoProvider> addBill(@Body UserSsoProvider userProviderModel);

    //user
    @GET(Constants.USER_URL)
    Call<UserSso[]> getUserInfo();

    //provider
    @GET(Constants.PROVIDER_URL)
    Call<Provider[]> getProviderInfo();

    //bill details
    @GET(Constants.BILL_DETAILS_URL)
    Call<BillDetails[]> getBillDetails(@Query("billId") String billId);

    //download file from given URL
    @Streaming
    @GET
    Call<ResponseBody> downloadFileFromUrl(@Url String fileUrl);


    @POST(Constants.URL_PURCHASE)
    Call<Purchase> proceedToPayment(@Body FEPurchase purchase);

    //Log out
    @POST(Constants.URL_LOGOUT)
    Call<Boolean> logout();

    //get telekom invoice
    @Streaming
    @GET(Constants.URL_INVOICE_TELEKOM)
    Call<ResponseBody> getTelekomInvoice(@Query("invoiceNumber") String invoiceNumber);

    //get user active providers
    @GET(Constants.ADD_BILL_URL)
    Call<UserSsoProvider[]> getActiveProviders();

    *//*
    DELETE CALLS
     *//*

    //remove card
    @DELETE(Constants.URL_CARD)
    Call<ResponseBody> deleteCard(@Query("cardId") Long cardID);

    //remove device
    @DELETE(Constants.URL_DEVICE)
    Call<ResponseBody> deleteDevice(@Query("deviceId") Long deviceId);

    //delete active provider
    @DELETE (Constants.ADD_BILL_URL)
    Call<ResponseBody> deleteActiveProvider(@Query("userProviderId") Long providerId);

    ///////*/

}
