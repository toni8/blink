package com.baf.blink.network;


import android.app.Dialog;
import android.content.Context;

import com.baf.blink.utils.UiHelper;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GenericCallback<T> implements Callback<T> {

    private Dialog progressDialog;
    private Context ctx;

    public GenericCallback(Context ctx) {
        this.ctx = ctx;

        progressDialog = UiHelper.createLoadingDialog(ctx, false);
        progressDialog.show();
    }

    @Override
    public void onResponse(Call<T> call, Response<T> response) {
        if (progressDialog != null)
            progressDialog.dismiss();
    }

    @Override
    public void onFailure(Call<T> call, Throwable t) {
        if (progressDialog != null)
            progressDialog.dismiss();
    }
}