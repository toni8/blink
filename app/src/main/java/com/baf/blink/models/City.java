package com.baf.blink.models;

import static android.os.Build.ID;

/**
 * Created by Antonio on 20/02/2017.
 */

public class City {
    String name;
    String _id;
    String lastEditedDate;
    Location location;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getID() {
        return _id;
    }

    public void setID(String _id) {
        this._id = _id;
    }

    public String getEditedDate() {
        return lastEditedDate;
    }

    public void setEditedDate(String editedDate) {
        this.lastEditedDate = lastEditedDate;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }
}
