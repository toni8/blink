package com.baf.blink.models;

import java.util.List;

/**
 * Created by Antonio on 15/03/2017.
 */

public class ProductOrder {

    String _id;
    float quantity;

    public ProductOrder(String _id, float quantity) {
        this._id = _id;
        this.quantity = quantity;
    }

    public String getId() {
        return _id;
    }

    public void setId(String _id) {
        this._id = _id;
    }

    public float getQuantity() {
        return quantity;
    }

    public void setQuantity(float quantity) {
        this.quantity = quantity;
    }
}
