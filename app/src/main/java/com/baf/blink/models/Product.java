package com.baf.blink.models;

import java.util.Date;

import static android.R.attr.name;

/**
 * Created by Antonio on 01/03/2017.
 */

public class Product {
    String _id;
    String name;
    String picture;
    String measureUnit;
    int stock;
    int price;
    int promoPrice;
    Date lastEditedDate;
    float quantity;

    public float getQuantity() {
        return quantity;
    }

    public void setQuantity(float quantity) {
        this.quantity = quantity;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getMeasureUnit() {
        if (measureUnit.equalsIgnoreCase("P"))
            return "Парче";
        else if (measureUnit.equalsIgnoreCase("P"))
            return "Кг";
        else
            return measureUnit;
    }

    public void setMeasureUnit(String measureUnit) {
        this.measureUnit = measureUnit;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getPromoPrice() {
        return promoPrice;
    }

    public void setPromoPrice(int promoPrice) {
        this.promoPrice = promoPrice;
    }

    public Date getLastEditedDate() {
        return lastEditedDate;
    }

    public void setLastEditedDate(Date lastEditedDate) {
        this.lastEditedDate = lastEditedDate;
    }
}
