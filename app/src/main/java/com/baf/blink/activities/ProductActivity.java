package com.baf.blink.activities;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.baf.blink.R;
import com.baf.blink.adapters.ProductCardItemAdapter;
import com.baf.blink.adapters.ProductsAdapter;
import com.baf.blink.models.Product;
import com.baf.blink.network.GenericCallback;
import com.baf.blink.persistance.AppHolder;
import com.baf.blink.utils.UiHelper;

import java.util.ArrayList;
import java.util.Arrays;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.baf.blink.R.id.recyclerView;

public class ProductActivity extends BaseActivity {


    ArrayList<Product> products;
    ProductsAdapter productAdapter;
    TextView tvTotalCost;
    ImageButton ivShop;
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_products);

        tvTotalCost = (TextView) findViewById(R.id.tv_total_cost);
        ivShop = (ImageButton) findViewById(R.id.fab);

        products = new ArrayList<>();
        productAdapter = new ProductsAdapter(this, products, tvTotalCost, ivShop);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(productAdapter);
        getProducts();

//        GridView gridView = (GridView) findViewById(R.id.grid_view);
//        gridView.setAdapter(productAdapter);

    }


    private void getProducts() {

        Call<Product[]> call = AppHolder.getInstance().getClientInterface().getProducts();
        call.enqueue(new GenericCallback<Product[]>(this) {
            @Override
            public void onResponse(Call<Product[]> call, Response<Product[]> response) {
                super.onResponse(call, response);
                if (response.body() != null && response.body().length > 0) {
                    products.addAll(Arrays.asList(response.body()));
                }

                productAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<Product[]> call, Throwable t) {
                super.onFailure(call, t);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        productAdapter.notifyDataSetChanged();
    }
}
