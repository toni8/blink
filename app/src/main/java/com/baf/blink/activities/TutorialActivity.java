package com.baf.blink.activities;


import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.baf.blink.R;

public class TutorialActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tutorial);

        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);

        ViewPager viewpager = (ViewPager) findViewById(R.id.pager);
        MyPagerAdapter adapter = new MyPagerAdapter();
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabDots);
        tabLayout.setupWithViewPager(viewpager, true);
        viewpager.setAdapter(adapter);

    }

    class MyPagerAdapter extends PagerAdapter {

        int[] layouts = {R.layout.tutorial_page_one, R.layout.tutorial_page_two, R.layout.tutorial_page_three,
        R.layout.tutorial_page_four};

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            LayoutInflater inflater = LayoutInflater.from(TutorialActivity.this);
            ViewGroup layout = (ViewGroup) inflater.inflate(layouts[position], container, false);
            if (position == 3) {
                layout.findViewById(R.id.btn_tutorial_start).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        startActivity(new Intent(TutorialActivity.this, CityActivity.class));
                        finish();
                    }
                });
            }
            container.addView(layout);
            return layout;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        @Override
        public int getCount() {
            return layouts.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

    }

}
