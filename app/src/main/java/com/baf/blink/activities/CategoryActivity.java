package com.baf.blink.activities;

import android.app.Dialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.baf.blink.BlinkSingleton;
import com.baf.blink.R;
import com.baf.blink.adapters.SimpleRecyclerViewAdapter;
import com.baf.blink.models.Category;
import com.baf.blink.models.City;
import com.baf.blink.network.GenericCallback;
import com.baf.blink.persistance.AppHolder;
import com.baf.blink.utils.Constants;
import com.baf.blink.utils.UiHelper;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CategoryActivity extends BaseActivity {

    RecyclerView rvCity;
    List<Object> categories;
    SimpleRecyclerViewAdapter simpleRecyclerViewAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_city);

        rvCity = (RecyclerView) findViewById(R.id.recycler_view_cities);

        categories = new ArrayList<>();
        simpleRecyclerViewAdapter = new SimpleRecyclerViewAdapter(this, R.layout.item_list_simple, categories);
        rvCity.setLayoutManager(new LinearLayoutManager(this));
        rvCity.setItemAnimator(new DefaultItemAnimator());
        rvCity.setAdapter(simpleRecyclerViewAdapter);
        rvCity.setLayoutManager(new LinearLayoutManager(this));

        if (BlinkSingleton.getInstance().getCategoryList().size() == 0) {
            Call<Category[]> call = AppHolder.getInstance().getClientInterface().getCategories();
            call.enqueue(new GenericCallback<Category[]>(this) {
                @Override
                public void onResponse(Call<Category[]> call, Response<Category[]> response) {
                    super.onResponse(call, response);
                    if (response.body() != null && response.body().length > 0) {
                        for (Category category : response.body()) {
                            BlinkSingleton.getInstance().addCategoryToList(category);
                            if (category.getParentCategory() == null)
                                categories.add(category);
                        }
                    }

                    simpleRecyclerViewAdapter.notifyDataSetChanged();
                }

                @Override
                public void onFailure(Call<Category[]> call, Throwable t) {
                    super.onFailure(call, t);
                    new SweetAlertDialog(CategoryActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                            .setTitleText("Нарачката е завршена")
                            .setContentText("Вашите продукти ќе бидат доставени во 15:00 часот")
                            .show();

                }
            });
        } else {
            for (Category category : BlinkSingleton.getInstance().getCategoryList()) {
                if (category.getParentCategory() == null)
                    categories.add(category);
            }
            simpleRecyclerViewAdapter.notifyDataSetChanged();
        }
    }
}
