package com.baf.blink.activities;

import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;
import android.widget.TextView;

import com.baf.blink.BlinkSingleton;
import com.baf.blink.R;
import com.baf.blink.adapters.CartAdapter;
import com.baf.blink.models.Product;

import java.util.List;

public class CartActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    List<Product> products;
    CartAdapter cartAdapter;
    TextView totalCostTv;
    ImageView ivShop;

    public static final String EXTRA_TOTAL_COST = "extra_total_cost";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);

        products = BlinkSingleton.getInstance().getCartProducts();

        String totalCost = getIntent().getStringExtra(EXTRA_TOTAL_COST);

        totalCostTv = (TextView) findViewById(R.id.tv_total_cost);
        totalCostTv.setText(totalCost);

        ivShop = (ImageView) findViewById(R.id.fab);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        cartAdapter = new CartAdapter(this, products, totalCostTv, ivShop);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(cartAdapter);

    }
}
