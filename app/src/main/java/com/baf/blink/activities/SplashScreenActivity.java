package com.baf.blink.activities;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;

import com.baf.blink.R;

public class SplashScreenActivity extends AppCompatActivity implements Animation.AnimationListener {

    private Handler mHandler = new Handler();
    Animation bounceAnim;
    ImageView ivSplashLogo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        bounceAnim = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.anim_bounce);
        bounceAnim.setAnimationListener(this);

        ivSplashLogo = (ImageView) findViewById(R.id.iv_splash_logo);
        ivSplashLogo.startAnimation(bounceAnim);

    }

    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {

        mHandler.postDelayed(new Runnable() {
            public void run() {
                startActivity(new Intent(SplashScreenActivity.this, TutorialActivity.class));
                finish();
            }
        }, 500);
    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }
}
