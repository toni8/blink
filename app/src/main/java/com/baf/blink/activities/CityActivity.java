package com.baf.blink.activities;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.baf.blink.R;
import com.baf.blink.adapters.SimpleRecyclerViewAdapter;
import com.baf.blink.models.City;
import com.baf.blink.network.GenericCallback;
import com.baf.blink.persistance.AppHolder;
import com.baf.blink.utils.UiHelper;

import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import com.baf.blink.utils.Constants;

public class CityActivity extends BaseActivity {

    RecyclerView rvCity;
    ArrayList<Object> cities;
    SimpleRecyclerViewAdapter cityCustomAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_city);

        rvCity = (RecyclerView) findViewById(R.id.recycler_view_cities);

        cities = new ArrayList<>();
        cityCustomAdapter = new SimpleRecyclerViewAdapter(this, R.layout.item_list_simple,  cities);
        rvCity.setLayoutManager(new LinearLayoutManager(this));
        rvCity.setItemAnimator(new DefaultItemAnimator());
        rvCity.setAdapter(cityCustomAdapter);
        rvCity.setLayoutManager(new LinearLayoutManager(this));

        Call<City[]> call = AppHolder.getInstance().getClientInterface().getCities();
        call.enqueue(new GenericCallback<City[]>(this) {
            @Override
            public void onResponse(Call<City[]> call, Response<City[]> response) {
                super.onResponse(call, response);

                if(response.body() != null && response.body().length > 0){
                    for(City city : response.body()){
                        cities.add(city);
                    }
                }

                cityCustomAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<City[]> call, Throwable t) {
                super.onFailure(call, t);

                new SweetAlertDialog(CityActivity.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Грешка")
                        .setContentText("Настана грешка при читање на податоци")
                        .show();

            }
        });
    }

}
