package com.baf.blink.activities;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.TextView;

import com.baf.blink.R;
import com.baf.blink.adapters.ProductsAdapter;
import com.baf.blink.models.Product;
import com.baf.blink.network.GenericCallback;
import com.baf.blink.persistance.AppHolder;

import java.util.ArrayList;
import java.util.Arrays;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Response;

public class ProductActivityBackup extends BaseActivity {


    ArrayList<Product> products;
    ProductsAdapter productAdapter;
    TextView tvTotalCost;
    ImageButton ivShop;
    private GridLayoutManager lLayout;
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_products);

        tvTotalCost = (TextView) findViewById(R.id.tv_total_cost);
        ivShop = (ImageButton) findViewById(R.id.fab);

        ivShop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createDialog(ProductActivityBackup.this, true).show();
            }
        });

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        lLayout = new GridLayoutManager(this, 3);
        products = new ArrayList<>();
        productAdapter = new ProductsAdapter(this, products, tvTotalCost);
        recyclerView.setLayoutManager(lLayout);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(productAdapter);
/*        GridView gridView = (GridView) findViewById(R.id.grid_view);
        gridView.setAdapter(productAdapter);*/
        getProducts();

    }


    private void getProducts() {

        Call<Product[]> call = AppHolder.getInstance().getClientInterface().getProducts();
        call.enqueue(new GenericCallback<Product[]>(this) {
            @Override
            public void onResponse(Call<Product[]> call, Response<Product[]> response) {
                super.onResponse(call, response);
                if (response.body() != null && response.body().length > 0) {
                    products.addAll(Arrays.asList(response.body()));
                }

                productAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<Product[]> call, Throwable t) {
                super.onFailure(call, t);
            }
        });
    }


    public Dialog createDialog(Context ctx, Boolean cancelable) {

        View v = View.inflate(ctx, R.layout.custom_dialog_view, null);

        final Dialog dialog = new Dialog(ctx);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (dialog.getWindow() != null)
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(v);
        dialog.setCancelable(cancelable);
        dialog.setCanceledOnTouchOutside(cancelable);

        v.findViewById(R.id.btn_dialog_accept).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                new SweetAlertDialog(ProductActivityBackup.this, SweetAlertDialog.SUCCESS_TYPE)
                        .setTitleText("Успешно направена нарачка")
                        .setConfirmText("Затвори")
                        .show();
            }
        });

        v.findViewById(R.id.btn_dialog_reject).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new SweetAlertDialog(ProductActivityBackup.this, SweetAlertDialog.WARNING_TYPE)
                        .setTitleText("Дали сте сигурни?")
                        .setContentText("Моменталната нарачка ќе бидео откажана.")
                        .setCancelText("Не")
                        .setConfirmText("Да")
                        .showCancelButton(true)
                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.cancel();
                            }
                        })
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismiss();
                                new SweetAlertDialog(ProductActivityBackup.this, SweetAlertDialog.SUCCESS_TYPE)
                                        .setTitleText("Нарачката успешно откажана")
                                        .setConfirmText("Затвори")
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                                startActivity(new Intent(ProductActivityBackup.this, CityActivity.class));
                                                finish();
                                            }
                                        })
                                        .show();
                            }
                        })
                        .show();
            }
        });

        return dialog;
    }

}
