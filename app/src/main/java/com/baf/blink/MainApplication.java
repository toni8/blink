package com.baf.blink;


import android.app.Application;

import com.baf.blink.network.Client;
import com.baf.blink.network.ClientInterface;
import com.baf.blink.persistance.AppHolder;

public class MainApplication extends Application {

    public static final String LOG_TAG = "mWallet";
    public static final boolean isTest = true;

    @Override
    public void onCreate() {
        super.onCreate();
        AppHolder.getInstance().setClientInterface(Client.getClient(getApplicationContext().getString(R.string.base_url), false).create(ClientInterface.class));

    }
/*

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleHelper.onAttach(base, "en"));
    }
*/


}
