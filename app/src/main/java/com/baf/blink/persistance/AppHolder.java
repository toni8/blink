package com.baf.blink.persistance;


import android.content.Context;

import com.baf.blink.network.ClientInterface;

public class AppHolder {

    private static AppHolder instance = null;

    private Context context;
    private ClientInterface clientInterface = null;

    private AppHolder(){

    }

    public static AppHolder getInstance() {
        if(instance == null){
            instance = new AppHolder();
        }
        return instance;
    }

    public ClientInterface getClientInterface() {
        return clientInterface;
    }

    public void setClientInterface(ClientInterface clientInterface) {
        this.clientInterface = clientInterface;
    }
}
