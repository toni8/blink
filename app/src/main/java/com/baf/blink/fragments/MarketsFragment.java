package com.baf.blink.fragments;

import android.app.Dialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import com.baf.blink.R;
import com.baf.blink.adapters.CardItemAdapter;
import com.baf.blink.models.City;
import com.baf.blink.models.Market;
import com.baf.blink.network.GenericCallback;
import com.baf.blink.persistance.AppHolder;
import com.baf.blink.utils.Constants;
import com.baf.blink.utils.UiHelper;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MarketsFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MarketsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MarketsFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    ArrayList<Market> markets;
    CardItemAdapter cardItemAdapter;

    private OnFragmentInteractionListener mListener;

    public MarketsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MarketsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MarketsFragment newInstance(String param1, String param2) {
        MarketsFragment fragment = new MarketsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public static MarketsFragment newInstance() {
        return new MarketsFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.grid_layout, container, false);
        GridView gridView = (GridView) view.findViewById(R.id.grid_view);

        markets = new ArrayList<>();
        cardItemAdapter = new CardItemAdapter(getActivity(), markets);
        gridView.setAdapter(cardItemAdapter);

        getMarkets();

        return view;
    }

    private void getMarkets() {

        String cityID = getActivity().getIntent().getStringExtra(Constants.EXTRA_CITY_ID);

        Call<Market[]> call = AppHolder.getInstance().getClientInterface().getMarketsByCity(cityID);
        call.enqueue(new GenericCallback<Market[]>(getActivity()) {
            @Override
            public void onResponse(Call<Market[]> call, Response<Market[]> response) {
                super.onResponse(call, response);
                if (response.body() != null && response.body().length > 0) {
                    for (Market market : response.body()) {
                        markets.add(market);
                    }
                }

                cardItemAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<Market[]> call, Throwable t) {
                super.onFailure(call, t);
            }
        });
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
