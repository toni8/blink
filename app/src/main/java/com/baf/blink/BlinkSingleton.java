package com.baf.blink;

import com.baf.blink.models.Category;
import com.baf.blink.models.Product;

import java.util.ArrayList;
import java.util.List;

public class BlinkSingleton {

    List<Category> categoryList;
    List<Product> cartProducts;

    private static BlinkSingleton instance = null;

    private BlinkSingleton() {
    }

    public static BlinkSingleton getInstance() {

        if (instance == null) {
            instance = new BlinkSingleton();
        }
        return instance;
    }

    public void addCategoryToList(Category c) {
        if(categoryList == null)
            categoryList = new ArrayList<>();

        categoryList.add(c);
    }

    public List<Category> getCategoryList() {
        if (categoryList == null)
            categoryList = new ArrayList<>();
        return categoryList;
    }

    public List<Product> getCartProducts() {
        if(cartProducts == null)
            cartProducts = new ArrayList<>();
        return cartProducts;
    }

    public void setCartProducts(List<Product> cartProducts) {
        this.cartProducts = cartProducts;
    }

    public void removeCartProduct(Product p){
        if(cartProducts == null)
            cartProducts = new ArrayList<>();
        cartProducts.remove(p);
    }

    public void addCartProduct(Product p){
        if(cartProducts == null)
            cartProducts = new ArrayList<>();
        cartProducts.add(p);
    }

    public void setCategoryList(List<Category> categoryList) {
        this.categoryList = categoryList;
    }
}
