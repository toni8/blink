package com.baf.blink.utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.baf.blink.R;

/**
 * Created by Antonio on 17-Feb-17.
 */

public class UiHelper {


    public static void setupActionBar(AppCompatActivity act) {
        ActionBar ab = act.getSupportActionBar();
        if (ab != null) {

            ab.setDisplayShowCustomEnabled(true);
            ab.setDisplayShowTitleEnabled(false);
            ab.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
            ab.setCustomView(R.layout.action_bar_head);


         /*   LayoutInflater inflator = (LayoutInflater) act
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View v = inflator.inflate(R.layout.action_bar_head, null);
            ActionBar.LayoutParams layoutParams = new ActionBar.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT);
            layoutParams.gravity = Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL;
            ab.setCustomView(v, layoutParams);*/
        }
    }

    /**
     * Creates custom circle green progress spinner dialog
     *
     * @param ctx context
     * @param cancelable true to make the dialog cancelable, false otherwise
     * @return the dialog
     */

    public static Dialog createLoadingDialog(Context ctx, Boolean cancelable) {

        View v = View.inflate(ctx, R.layout.custom_progress_dialog, null);

        Dialog dialog = new Dialog(ctx);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (dialog.getWindow() != null)
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(v);
        dialog.setCancelable(cancelable);
        dialog.setCanceledOnTouchOutside(cancelable);

        Animation rotation = AnimationUtils.loadAnimation(ctx, R.anim.rotate_anim);
        rotation.setFillAfter(true);
        v.startAnimation(rotation);

        return dialog;
    }

    public static Dialog createLoadingDialog(Activity ctx){
        return createLoadingDialog(ctx, false);
    }
}
