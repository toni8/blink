package com.baf.blink.utils;

/**
 * Created by Antonio on 16/03/2017.
 */

public class Utils {

    public static String formatFloat(float d)
    {
        if(d == (long) d)
            return String.format("%d",(long)d);
        else
            return String.format("%s",d);
    }
}
