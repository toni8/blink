package com.baf.blink.utils;

/**
 * Created by Antonio on 20/02/2017.
 */

public class Constants {

    public static final String URL_MARKETS_BY_CITY = "markets/by-city";
    public static final String URL_CITIES = "cities";
    public static final String URL_CATEGORIES = "categories";
    public static final String URL_PRODUCTS = "products";
    public static final String URL_ORDER = "orders/new";

    public static final String EXTRA_CITY_ID = "extra_city_id";

    public static final String OBJECT_CITY = "City";
    public static final String OBJECT_CATEGORY = "Category";
}
