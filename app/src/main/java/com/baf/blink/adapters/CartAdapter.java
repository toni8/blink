package com.baf.blink.adapters;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.baf.blink.BlinkSingleton;
import com.baf.blink.R;
import com.baf.blink.activities.CartActivity;
import com.baf.blink.activities.CityActivity;
import com.baf.blink.models.Order;
import com.baf.blink.models.Product;
import com.baf.blink.models.ProductOrder;
import com.baf.blink.network.GenericCallback;
import com.baf.blink.persistance.AppHolder;
import com.baf.blink.utils.Utils;
import com.bumptech.glide.Glide;

import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by Antonio on 15/03/2017.
 */

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.MyViewHolder> {

    private List<Product> productList;
    private Context ctx;
    private TextView tvTotalCost;
    private int totalCost = 0;
    private Map<String, Float> productOrdersMap = new HashMap<>();
    private List<ProductOrder> productOrders = new ArrayList<>();
    final ImageView ivShop;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        final ImageView imageView;

        final TextView titleTv;
        final TextView priceTv;
        final TextView increaseTv;
        final TextView decreaseTv;
        final TextView quantityTv;
        final TextView costTv;

        public MyViewHolder(View view) {
            super(view);
            imageView = (ImageView) view.findViewById(R.id.iv_cart_img);
            titleTv = (TextView) view.findViewById(R.id.tv_cart_title);
            priceTv = (TextView) view.findViewById(R.id.tv_cart_price);
            increaseTv = (TextView) view.findViewById(R.id.tv_cart_increase);
            decreaseTv = (TextView) view.findViewById(R.id.tv_cart_decrease);
            quantityTv = (TextView) view.findViewById(R.id.tv_cart_quantity);
            costTv = (TextView) view.findViewById(R.id.tv_cart_cost);
        }
    }


    public CartAdapter(final Context ctx, List<Product> productList, final TextView tvTotalCost, ImageView ivShop) {
        this.ctx = ctx;
        this.productList = productList;
        this.tvTotalCost = tvTotalCost;
        this.ivShop = ivShop;

        totalCost  = Integer.valueOf(tvTotalCost.getText().toString().split(" ")[0]);
        ivShop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createDialog(ctx, true).show();
            }
        });
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_cart, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final Product product = productList.get(position);
        Glide.with(ctx)
                .load(ctx.getApplicationContext().getString(R.string.base_url) + product.getPicture())
                .into(holder.imageView);
        holder.titleTv.setText(product.getName());
        holder.priceTv.setText(String.valueOf(product.getPrice()) + " МКД" + "/" + product.getMeasureUnit());
        holder.quantityTv.setText(Utils.formatFloat(product.getQuantity()));
        holder.costTv.setText(String.valueOf(product.getPrice()*product.getQuantity()));

        holder.increaseTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (product.getMeasureUnit().equalsIgnoreCase("KG")) {
                    product.setQuantity(round(product.getQuantity(), 2) + 0.05f);
                    holder.quantityTv.setText(String.valueOf(round(product.getQuantity(), 2)) + " kg");

                    totalCost += 0.05f * product.getPrice();
                    tvTotalCost.setText(String.valueOf(totalCost));

                } else {
                    product.setQuantity(product.getQuantity() + 1);
                    holder.quantityTv.setText(String.valueOf((int) product.getQuantity()));

                    totalCost += product.getPrice();
                    tvTotalCost.setText(String.valueOf(totalCost) + " МКД");
                }

                productOrdersMap.put(product.get_id(), product.getQuantity());

                holder.costTv.setText(String.valueOf(product.getPrice()*product.getQuantity()));
            }
        });


        holder.decreaseTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (product.getMeasureUnit().equalsIgnoreCase("KG")) {
                    if (product.getQuantity() != 0) {
                        product.setQuantity(round(product.getQuantity(), 2) - 0.05f);
                        holder.quantityTv.setText(String.valueOf(round(product.getQuantity(), 2)) + " кг");

                        totalCost -= 0.05f * product.getPrice();
                        tvTotalCost.setText(String.valueOf(totalCost));
                    }

                } else {
                    if (product.getQuantity() != 0) {
                        product.setQuantity(product.getQuantity() - 1);
                        holder.quantityTv.setText(String.valueOf((int) product.getQuantity()));

                        totalCost -= product.getPrice();
                        tvTotalCost.setText(String.valueOf(totalCost) + " МКД");
                    }
                }

                productOrdersMap.put(product.get_id(), product.getQuantity());

                if (product.getQuantity() == 0) {
                    productOrdersMap.remove(product.get_id());
                }

                holder.costTv.setText(String.valueOf(product.getPrice()*product.getQuantity()));
            }
        });

        if (product.getMeasureUnit().equalsIgnoreCase("KG"))
            holder.quantityTv.setText(Utils.formatFloat(product.getQuantity()) + " кг");
        else
            holder.quantityTv.setText(Utils.formatFloat(product.getQuantity()));

    }

    @Override
    public int getItemCount() {
        return productList.size();
    }



    public Dialog createDialog(final Context ctx, Boolean cancelable) {

        View v = View.inflate(ctx, R.layout.custom_dialog_view, null);

        final Dialog dialog = new Dialog(ctx);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (dialog.getWindow() != null)
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(v);
        dialog.setCancelable(cancelable);
        dialog.setCanceledOnTouchOutside(cancelable);

        v.findViewById(R.id.btn_dialog_accept).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                sendOrder();
                new SweetAlertDialog(ctx, SweetAlertDialog.SUCCESS_TYPE)
                        .setTitleText("Успешно направена нарачка")
                        .setConfirmText("Затвори")
                        .show();
            }
        });

        v.findViewById(R.id.btn_dialog_reject).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new SweetAlertDialog(ctx, SweetAlertDialog.WARNING_TYPE)
                        .setTitleText("Дали сте сигурни?")
                        .setContentText("Моменталната нарачка ќе бидео откажана.")
                        .setCancelText("Не")
                        .setConfirmText("Да")
                        .showCancelButton(true)
                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.cancel();
                            }
                        })
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismiss();
                                new SweetAlertDialog(ctx, SweetAlertDialog.SUCCESS_TYPE)
                                        .setTitleText("Нарачката успешно откажана")
                                        .setConfirmText("Затвори")
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                                ctx.startActivity(new Intent(ctx, CityActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP));
                                            }
                                        })
                                        .show();
                            }
                        })
                        .show();
            }
        });

        return dialog;
    }

    private void sendOrder() {
        Order order = new Order();
        order.setAddress("Adresa");
        order.setCustomerName("Antonio");
        order.setPhoneNumber("123124124");
        List<String> ids = new ArrayList<>(productOrdersMap.keySet());
        List<Float> quantities = new ArrayList<>(productOrdersMap.values());
        for(int i = 0; i < ids.size(); i++){
            if(quantities.get(i) != null) {
                ProductOrder productOrder = new ProductOrder(ids.get(i), quantities.get(i));
                productOrders.add(productOrder);
            }
        }
        order.setProducts(productOrders);
        Call<ResponseBody> sendBillsToCasys = AppHolder.getInstance().getClientInterface().postOrder(order);
        sendBillsToCasys.enqueue(new GenericCallback<ResponseBody>(ctx) {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                super.onResponse(call, response);
                productOrders.clear();

                if (response.code() == HttpURLConnection.HTTP_OK) {
                    Toast.makeText(ctx, "success", Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                super.onFailure(call, t);
                productOrders.clear();
                Toast.makeText(ctx, "fail", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public static float round(float d, int decimalPlace) {
        BigDecimal bd = new BigDecimal(Float.toString(d));
        bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);
        return bd.floatValue();
    }

}