package com.baf.blink.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Movie;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.baf.blink.BlinkSingleton;
import com.baf.blink.R;
import com.baf.blink.activities.CartActivity;
import com.baf.blink.models.Product;
import com.baf.blink.models.ProductOrder;
import com.baf.blink.utils.Utils;
import com.bumptech.glide.Glide;

import org.w3c.dom.Text;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Antonio on 15/03/2017.
 */

public class ProductsAdapter extends RecyclerView.Adapter<ProductsAdapter.MyViewHolder> {

    private List<Product> productList;
    private Context ctx;
    private TextView tvTotalCost;
    private int totalCost = 0;
    private Map<String, Float> productOrdersMap = new HashMap<>();
    private List<ProductOrder> productOrders = new ArrayList<>();

    public class MyViewHolder extends RecyclerView.ViewHolder {

        final ImageView imageView;
        final TextView titleTv;
        final TextView priceTv;
        final TextView increaseTv;
        final TextView decreaseTv;
        final TextView quantityTv;
        final TextView costTv;

        public MyViewHolder(View view) {
            super(view);
            imageView = (ImageView) view.findViewById(R.id.iv_cart_img);
            titleTv = (TextView) view.findViewById(R.id.tv_cart_title);
            priceTv = (TextView) view.findViewById(R.id.tv_cart_price);
            increaseTv = (TextView) view.findViewById(R.id.tv_cart_increase);
            decreaseTv = (TextView) view.findViewById(R.id.tv_cart_decrease);
            quantityTv = (TextView) view.findViewById(R.id.tv_cart_quantity);
            costTv = (TextView) view.findViewById(R.id.tv_cart_cost);
        }
    }


    public ProductsAdapter(final Context ctx, List<Product> productList, final TextView tvTotalCost, ImageView ivShop) {
        this.ctx = ctx;
        this.productList = productList;
        this.tvTotalCost = tvTotalCost;

        ivShop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                for (String id : productOrdersMap.keySet()) {
                    for (Product p : BlinkSingleton.getInstance().getCartProducts()) {
                        if (p.get_id().equalsIgnoreCase(id)) {
                            BlinkSingleton.getInstance().getCartProducts()
                                    .get(BlinkSingleton.getInstance().getCartProducts().indexOf(p))
                                    .setQuantity(productOrdersMap.get(id));
                        }
                    }
                }
                ctx.startActivity(new Intent(ctx, CartActivity.class).putExtra(CartActivity.EXTRA_TOTAL_COST, tvTotalCost.getText().toString()));
                // createDialog(ctx, true).show();
            }
        });

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_cart, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final Product product = productList.get(position);
        Glide.with(ctx)
                .load(ctx.getApplicationContext().getString(R.string.base_url) + product.getPicture())
                .into(holder.imageView);
        holder.titleTv.setText(product.getName());
        holder.priceTv.setText(String.valueOf(product.getPrice()) + " МКД" + "/" + product.getMeasureUnit());
        holder.quantityTv.setText(Utils.formatFloat(product.getQuantity()));
        holder.costTv.setText(String.valueOf(product.getPrice()*product.getQuantity()));

        holder.increaseTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (product.getMeasureUnit().equalsIgnoreCase("KG")) {
                    product.setQuantity(round(product.getQuantity(), 2) + 0.05f);
                    holder.quantityTv.setText(String.valueOf(round(product.getQuantity(), 2)) + " kg");

                    totalCost += 0.05f * product.getPrice();
                    tvTotalCost.setText(String.valueOf(totalCost));

                } else {
                    product.setQuantity(product.getQuantity() + 1);
                    holder.quantityTv.setText(String.valueOf((int) product.getQuantity()));

                    totalCost += product.getPrice();
                    tvTotalCost.setText(String.valueOf(totalCost) + " МКД");
                }

                productOrdersMap.put(product.get_id(), product.getQuantity());

                if (product.getQuantity() == 1 || product.getQuantity() == 0.05f) {
                    BlinkSingleton.getInstance().addCartProduct(product);
                }

                holder.costTv.setText(String.valueOf(product.getPrice()*product.getQuantity()));
            }
        });


        holder.decreaseTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (product.getMeasureUnit().equalsIgnoreCase("KG")) {
                    if (product.getQuantity() != 0) {
                        product.setQuantity(round(product.getQuantity(), 2) - 0.05f);
                        holder.quantityTv.setText(String.valueOf(round(product.getQuantity(), 2)) + " кг");

                        totalCost -= 0.05f * product.getPrice();
                        tvTotalCost.setText(String.valueOf(totalCost));
                    }

                } else {
                    if (product.getQuantity() != 0) {
                        product.setQuantity(product.getQuantity() - 1);
                        holder.quantityTv.setText(String.valueOf((int) product.getQuantity()));

                        totalCost -= product.getPrice();
                        tvTotalCost.setText(String.valueOf(totalCost) + " МКД");
                    }
                }

                productOrdersMap.put(product.get_id(), product.getQuantity());

                if (product.getQuantity() == 0) {
                    productOrdersMap.remove(product.get_id());
                    BlinkSingleton.getInstance().removeCartProduct(product);
                }

                holder.costTv.setText(String.valueOf(product.getPrice()*product.getQuantity()));
            }
        });

        if (product.getMeasureUnit().equalsIgnoreCase("KG"))
            holder.quantityTv.setText(Utils.formatFloat(product.getQuantity()) + " кг");
        else
            holder.quantityTv.setText(Utils.formatFloat(product.getQuantity()));

    }


    public static float round(float d, int decimalPlace) {
        BigDecimal bd = new BigDecimal(Float.toString(d));
        bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);
        return bd.floatValue();
    }

    @Override
    public int getItemCount() {
        return productList.size();
    }
}