package com.baf.blink.adapters;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.baf.blink.BlinkSingleton;
import com.baf.blink.R;
import com.baf.blink.activities.CartActivity;
import com.baf.blink.activities.CategoryActivity;
import com.baf.blink.activities.CityActivity;
import com.baf.blink.activities.ProductActivity;
import com.baf.blink.models.Market;
import com.baf.blink.models.Order;
import com.baf.blink.models.Product;
import com.baf.blink.models.ProductOrder;
import com.baf.blink.network.GenericCallback;
import com.baf.blink.persistance.AppHolder;
import com.baf.blink.utils.Utils;
import com.bumptech.glide.Glide;

import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by Antonio on 19-Feb-17.
 */
public class ProductCardItemAdapter extends BaseAdapter {
    private Context ctx;
    private ArrayList<Product> markets;
    private TextView tvTotalCost;
    private int totalCost = 0;
    private ImageButton ivShop;
    private Map<String, Float> productOrdersMap = new HashMap<>();
    private List<ProductOrder> productOrders = new ArrayList<>();

    // Constructor
    public ProductCardItemAdapter(final Context ctx, ArrayList<Product> markets, final TextView tvTotalCost, ImageButton ivShop) {
        this.ctx = ctx;
        this.markets = markets;
        this.tvTotalCost = tvTotalCost;
        this.ivShop = ivShop;

        ivShop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                for (String id : productOrdersMap.keySet()) {
                    for (Product p : BlinkSingleton.getInstance().getCartProducts()) {
                        if (p.get_id().equalsIgnoreCase(id)) {
                            BlinkSingleton.getInstance().getCartProducts()
                                    .get(BlinkSingleton.getInstance().getCartProducts().indexOf(p))
                                    .setQuantity(productOrdersMap.get(id));
                        }
                    }
                }
                ctx.startActivity(new Intent(ctx, CartActivity.class).putExtra(CartActivity.EXTRA_TOTAL_COST, tvTotalCost.getText().toString()));
                // createDialog(ctx, true).show();
            }
        });
    }

    @Override
    public int getCount() {
        return markets.size();
    }

    @Override
    public Object getItem(int position) {
        return markets.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;

        if (convertView == null) {
            final LayoutInflater layoutInflater = LayoutInflater.from(ctx);
            convertView = layoutInflater.inflate(R.layout.item_card_product, parent, false);

            viewHolder = new ViewHolder();
            viewHolder.imageView = (ImageView) convertView.findViewById(R.id.iv_product_photo);
            viewHolder.titleTv = (TextView) convertView.findViewById(R.id.tv_product_name);
            viewHolder.priceTv = (TextView) convertView.findViewById(R.id.tv_product_price_unit);
            viewHolder.increaseTv = (TextView) convertView.findViewById(R.id.tv_product_increase);
            viewHolder.decreaseTv = (TextView) convertView.findViewById(R.id.tv_product_decrease);
            viewHolder.amountTv = (TextView) convertView.findViewById(R.id.tv_product_amount);

            convertView.setTag(viewHolder);


        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        final Product product = markets.get(position);

        Glide.with(ctx)
                .load(ctx.getApplicationContext().getString(R.string.base_url) + product.getPicture())
                .into(viewHolder.imageView);
        viewHolder.titleTv.setText(product.getName());
        viewHolder.priceTv.setText(String.valueOf(product.getPrice()) + " МКД" + "/" + product.getMeasureUnit());

        final ViewHolder finalViewHolder = viewHolder;
        viewHolder.increaseTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //  float amount = Float.valueOf( finalViewHolder.amountTv.getText().toString().split(" ")[0]);


                if (product.getMeasureUnit().equalsIgnoreCase("KG")) {
                    product.setQuantity(round(product.getQuantity(), 2) + 0.05f);
                    finalViewHolder.amountTv.setText(String.valueOf(round(product.getQuantity(), 2)) + " kg");

                    totalCost += 0.05f * product.getPrice();
                    tvTotalCost.setText(String.valueOf(totalCost));

                } else {
                    product.setQuantity(product.getQuantity() + 1);
                    finalViewHolder.amountTv.setText(String.valueOf((int) product.getQuantity()));

                    totalCost += product.getPrice();
                    tvTotalCost.setText(String.valueOf(totalCost) + " МКД");
                }

                productOrdersMap.put(product.get_id(), product.getQuantity());

                if (product.getQuantity() == 1 || product.getQuantity() == 0.05f) {
                    BlinkSingleton.getInstance().addCartProduct(product);
                }
            }
        });


        viewHolder.decreaseTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //       float amount = Float.valueOf( finalViewHolder.amountTv.getText().toString().split(" ")[0]);

                if (product.getMeasureUnit().equalsIgnoreCase("KG")) {
                    if (product.getQuantity() != 0) {
                        product.setQuantity(round(product.getQuantity(), 2) - 0.05f);
                        finalViewHolder.amountTv.setText(String.valueOf(round(product.getQuantity(), 2)) + " кг");

                        totalCost -= 0.05f * product.getPrice();
                        tvTotalCost.setText(String.valueOf(totalCost));
                    }

                } else {
                    if (product.getQuantity() != 0) {
                        product.setQuantity(product.getQuantity() - 1);
                        finalViewHolder.amountTv.setText(String.valueOf((int) product.getQuantity()));

                        totalCost -= product.getPrice();
                        tvTotalCost.setText(String.valueOf(totalCost) + " МКД");
                    }
                }

                productOrdersMap.put(product.get_id(), product.getQuantity());

                if (product.getQuantity() == 0) {
                    productOrdersMap.remove(product.get_id());
                    BlinkSingleton.getInstance().removeCartProduct(product);
                }
            }
        });

        if (product.getMeasureUnit().equalsIgnoreCase("KG"))
            finalViewHolder.amountTv.setText(Utils.formatFloat(product.getQuantity()) + " кг");
        else
            finalViewHolder.amountTv.setText(Utils.formatFloat(product.getQuantity()));

        return convertView;
    }

    public static float round(float d, int decimalPlace) {
        BigDecimal bd = new BigDecimal(Float.toString(d));
        bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);
        return bd.floatValue();
    }


    static class ViewHolder {

        ImageView imageView;
        TextView titleTv;
        TextView priceTv;
        TextView increaseTv;
        TextView decreaseTv;
        TextView amountTv;
    }

}