package com.baf.blink.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.baf.blink.R;
import com.baf.blink.activities.CategoryActivity;
import com.baf.blink.models.Market;

import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * Created by Antonio on 19-Feb-17.
 */
public class CardItemAdapter extends BaseAdapter {
    private Context mContext;
    private ArrayList<Market> markets;

    // Constructor
    public CardItemAdapter(Context c, ArrayList<Market> markets) {
        mContext = c;
        this.markets = markets;
    }

    @Override
    public int getCount() {
        return markets.size();
    }

    @Override
    public Object getItem(int position) {
        return markets.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Market market = markets.get(position);

        if (convertView == null) {
            final LayoutInflater layoutInflater = LayoutInflater.from(mContext);
            convertView = layoutInflater.inflate(R.layout.item_card, null);
        }

        final ImageView imageView = (ImageView) convertView.findViewById(R.id.iv_item_card);
        final TextView titleTv = (TextView) convertView.findViewById(R.id.tv_item_card);

      //  imageView.setImageResource(market.getPhoto());
        titleTv.setText(market.getName());

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mContext.startActivity(new Intent(mContext, CategoryActivity.class));
            }
        });
        return convertView;
    }

}