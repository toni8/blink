package com.baf.blink.adapters;

/**
 * Created by Antonio on 17/02/2017.
 */

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.baf.blink.BlinkSingleton;
import com.baf.blink.R;
import com.baf.blink.activities.MarketMerchantActivity;
import com.baf.blink.activities.ProductActivity;
import com.baf.blink.activities.TutorialActivity;
import com.baf.blink.models.Category;
import com.baf.blink.models.City;

import java.util.ArrayList;
import java.util.List;

import static com.baf.blink.utils.Constants.EXTRA_CITY_ID;


public class SimpleRecyclerViewAdapter extends RecyclerView.Adapter<SimpleRecyclerViewAdapter.ViewHolder> {



    private int listItemLayout;
    private List<Object> objectList = new ArrayList<>();
    private Activity activity;

    public SimpleRecyclerViewAdapter(Activity activity, int layoutId, List<Object> objectList) {
        this.activity = activity;
        listItemLayout = layoutId;
        this.objectList = objectList;
    }


    @Override
    public int getItemCount() {
        return objectList == null ? 0 : objectList.size();
    }


    // specify the row layout file and click for each row
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(listItemLayout, parent, false);
        ViewHolder myViewHolder = new ViewHolder(view);
        return myViewHolder;
    }

    // load data in each row element
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int listPosition) {
        TextView item = holder.item;
        String name = "";
        if (objectList.get(listPosition) instanceof Category)
           name = ((Category) objectList.get(listPosition)).getName();
        else if (objectList.get(listPosition) instanceof City)
            name = ((City) objectList.get(listPosition)).getName();

        item.setText(name);
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView item;

        public ViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            item = (TextView) itemView.findViewById(R.id.tv_item_title);
        }

        @Override
        public void onClick(View view) {
            if(objectList.get(getAdapterPosition()) instanceof Category){
                //add category click logic
                List<Object> newCategories = new ArrayList<>();
                Category selectedCategory = (Category) objectList.get(getAdapterPosition());
                for(Category category : BlinkSingleton.getInstance().getCategoryList()){
                    if((category.getParentCategory() != null &&
                            selectedCategory.get_id().equalsIgnoreCase(category.getParentCategory()))){
                        newCategories.add(category);
                    }
                }
                if(newCategories.size() == 0){
                    activity.startActivity(new Intent(activity, ProductActivity.class));
                }
                objectList.clear();
                objectList.addAll(newCategories);
                notifyDataSetChanged();
            } else if(objectList.get(getAdapterPosition()) instanceof City) {
                Log.d("onclick", "onClick " + getLayoutPosition() + " " + item.getText());

                Intent intent = new Intent(item.getContext(), MarketMerchantActivity.class);
                intent.putExtra(EXTRA_CITY_ID, ((City) objectList.get(getAdapterPosition())).getID());
                item.getContext().startActivity(intent);
            }
        }
    }
}